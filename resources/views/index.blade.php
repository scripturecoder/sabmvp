@extends('layouts.main')
@section('content')
    <section class="wrapper">
        <!--Start Slider-->
        <div class="slider-wrapper">
            <div class="slider">
                <div class="fs_loader"></div>

                <div class="slide slide1">
                    <img src="{{asset('images/fraction-slider/book.png')}}" width="300" height="400" data-position="50,370" data-in="right" data-out="left" data-delay="3500"/>
                    <img src="{{asset('images/fraction-slider/gadgets/smartphone.png')}}" width="70" height="140" data-position="125,840" data-in="top" data-out="fade" data-delay="4000"/>
                    <img src="{{asset('images/fraction-slider/gadgets/ipad.png')}}" width="180" height="270" data-position="70,950" data-in="bottom" data-out="bottom" data-delay="4500"/>


                    <p class="slide-heading" data-position="100,1200" dat-in="top" data-out="bottom" data-delay="500"><span> Life is the Master </span> Key</p>
                    <p class="slide-heading" data-position="160,1200" data-in="bottom" data-out="top" data-delay="1500">To Open Door into Life – <span>Hold the Key.</span></p>
                    <p class="slide-heading" data-position="220,1200" data-in="top" data-out="bottom" data-delay="2500"><span>Open </span> the <span>Door</span></p>
                    <p class="slide-heading" data-position="280,1200" data-in="bottom" data-out="top" data-delay="3500">And Discover <span>Essences of</span> Life... </p>
                </div>

                <div class="slide slide2">
                    <img src="{{asset('images/fraction-slider/base_2.jpg')}}" width="1920" height="auto" data-in="fade" data-out="fade" />
                    <img src="{{asset('images/fraction-slider/book.png')}}" width="300" height="auto" data-position="47,1200" data-in="bottomLeft" data-out="fade" style="width:auto; height:auto" data-delay="500">

                    <p class="slide-heading" data-position="130,380" data-in="top"  data-out="left" data-ease-in="easeOutBounce" data-delay="700">Bubbles and Ballons of Glory </p>
                    <p class="sub-line" data-position="220,380" data-in="right" data-out="left" data-delay="1500">A Fountain of Wisdom Speaks Understanding, Who will Listen?</p>
                    <a href=""	class="btn btn-default btn-lg" data-position="315,380" data-in="bottom" data-out="bottom" data-delay="2000">Buy Now</a>
                </div>

                <div class="slide slide3">
                    <img src="{{asset('images/fraction-slider/base_3.jpg')}}"  width="1920" height="auto" data-in="fade" data-out="fade" />

                    <img src="{{asset('images/fraction-slider/gadgets/ipad.png')}}" width="220" height="270" data-position="90,580" data-in="right" data-0ut="right" data-delay="700" >
                    <img src="{{asset('images/fraction-slider/gadgets/smartphone.png')}}" width="90" height="180" data-position="180,430" data-in="right" data-0ut="right" data-delay="1200" data-ease-in="easeOutBounce" transition="all 0.5s ease-in-out 0s">

                    <p class="slide-heading" data-position="80,950" data-in="top" data-out="fade" data-delay="3300" data-ease-in="easeOutBounce">Bubbles and Ballons of Glory </p>
                    <p class="sub-line" data-position="160,950" data-in="left" data-out="left" data-delay="4400">True, the Sea is DEEP –</p>
                    <p class="slide-text" data-position="200,950" data-in="left" data-out="left" data-delay="4600">but not like the Heart of Man –<br> Dear Reader, Discover Something... </p>
                    <a href=""	class="btn btn-default btn-lg" data-position="311,950" data-in="bottom" data-out="bottom" data-ease-in="easeOutBounce" data-delay="4800">Buy Now</a>
                </div>
            </div>
        </div>
        <!--End Slider-->

        <section class="promo_box dark  wow fadeInDown">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="promo_content">
                            <h3>Bubbles and balloons of Glory</h3>
                            <p>Read a Book – a well-Written Book. Discover a Path – a Honourable Path. Live a Lesson of Worthy Life...
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="pb_action">
                            <a class="btn btn-lg btn-default" href="#fakelink">
                                <i class="fa fa-shopping-cart"></i>
                                Buy Now
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="about" class="texture-section texture1">
            <div class="container">
                <div class="row sub_content">
                    <div class="who">
                        <div class="col-lg-8 col-md-8 col-sm-8">
                            <div class="dividerHeading">
                                <h4><span>About Sanmi-Ajiki</span></h4>
                            </div>
                            <img class="left_img img-thumbnail " src="{{asset('images/sab.jpg')}}" alt="about img">
                            <p style="text-align: justify; color: #06000c; font-size: 15px">HARVESTS-REVELATIONS-EXPOSITIONS... Life speaks out, revealing Life and Time is not silent, telling Time – An African Writer of outstanding distinction – Lolu Sanmi-Ajiki – novelist, poet, playwright, biographer, literary-journalist, prolific songwriter and singer, born-farmer and a happy fisherman – son of a farmer from Amoye-Ikere-Ekiti in Ekiti State of Nigeria – had worked with Central Bank and The Punch Newspaper in Lagos, Nigeria, as a Journalist – has been on his own either as a full-time Farmer, ardent Lover of Nature, in the hometown or and a peculiar professional Investor in the Art of Creative Writing based in Lagos – is contentedly doing his part nobly as an Observer and Interpreter of Life – communing, communicating and adventuring actively a Literary Explorer on explorations into the deepless Womb and Bosom of Life – scholarly tasked with the Great Commission to discover and unearth Deep Stories of Life by Time Extravaganzas – bountiful Mysteries of Life-via-Living Literature a Testimony ala Monument beautifully unravelling – excellently delicious bookish feasts to the Human World – as powered by his inimitably strong fundamental conviction: “Treasures of Life are hidden inside books, it is only the Reader who can discover them” – and perfectly fulfilled in the noble vocation by confirming this gold truism – Bubbles and Balloons of Glory… is his fifth published book. </p>

                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="dividerHeading">
                                <h4><span>Read The Blob</span></h4>
                            </div>
                            //
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {{--BLOB--}}
        <section id="blob" class="feature-block">
            <div class="container">
                <div class="row sub_content">
                    <div class="col-lg-6 wow fadeInLeft">
                        <div class="text-center">
                            <img src="{{asset('images/book.png')}}" alt=""/>
                        </div>
                    </div>

                    <div class="col-lg-6 fadeInRight">
                        <h4>Bubbles and balloons of Glory</h4>

                        <p class="w3-justify w3-white">BEHOLD A STORYLINE of GOLD! Tide and time: Viruses, vices and vanities war against virtues, voices and values. Any hope extract sweet juice from a grape of bitters? Say, worst plus waste is equal to wealth? A honey comb fetched from a stinking tomb? Gall galls, mud mutes, coal burns, silver twinkles and diamond sparkles but you are gold – how? Why? Where? When and for what reason? (Rags and Riches! Thorns and Crowns...) Bubbles and Balloons of Glory... (Sparkles of Paradise in the Wind!) Now, jolly crave a place in history? Long for a Story of Gold to all tell? In truth, the inglorious night gives birth to the glorious morning and dawn is the door: Door of Greatness... Want to chase away darkness? So simple: Switch on the light... For, alas, behold and lo: It is out of the amalgamation of flying, landing and perching dirks from fiery fires of cruel life and unkind time that record nobles and genius superstars, as enviable good successes and genuine victors, are made! Gem Beauties of Life: Prices they paid and prizes they win. More, their flowers flower. Their rainbows rainbow. Smooth their unique glories reel. Each of their names rings a booming bell. Their treasures flourish like lush green vegetation of rain. Loud their destinies speak. Spanking sparkling startling Stars of Glory out of Storms – brilliantly blissfully they give their lights, and dazzlingly joyfully they sing their telling songs. In rapturous ecstasy, drumbeats of celebration roll – who can and will indeed dance? Then, what then? Quick, don your hat of wisdom, knowledge and understanding. Fast, seize right the opportunity to be gold and you shall become gold: VISION IS THE MASTER KEY. Are you ready? Sure pretty ready? Good, beautiful! On your mark – set, go!</p>

                        <a href="" class="btn btn-default btn-lg" data-toggle="modal" data-target="#myModal">Read Book</a>&nbsp;
                        <a href="" class="btn btn-default btn-lg">Purchase Now</a>
                    </div>
                </div>
            </div>

            {{--read more modal--}}
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Bubbles and balloons of Glory</h4>
                        </div>
                        <div class="modal-body">
                            <p class="w3-justify w3-white" style="font-size: 15px">BEHOLD A STORYLINE of GOLD! Tide and time: Viruses, vices and vanities war against virtues, voices and values. Any hope extract sweet juice from a grape of bitters? Say, worst plus waste is equal to wealth? A honey comb fetched from a stinking tomb? Gall galls, mud mutes, coal burns, silver twinkles and diamond sparkles but you are gold – how? Why? Where? When and for what reason? (Rags and Riches! Thorns and Crowns...) Bubbles and Balloons of Glory... (Sparkles of Paradise in the Wind!) Now, jolly crave a place in history? Long for a Story of Gold to all tell? In truth, the inglorious night gives birth to the glorious morning and dawn is the door: Door of Greatness... Want to chase away darkness? So simple: Switch on the light... For, alas, behold and lo: It is out of the amalgamation of flying, landing and perching dirks from fiery fires of cruel life and unkind time that record nobles and genius superstars, as enviable good successes and genuine victors, are made! Gem Beauties of Life: Prices they paid and prizes they win. More, their flowers flower. Their rainbows rainbow. Smooth their unique glories reel. Each of their names rings a booming bell. Their treasures flourish like lush green vegetation of rain. Loud their destinies speak. Spanking sparkling startling Stars of Glory out of Storms – brilliantly blissfully they give their lights, and dazzlingly joyfully they sing their telling songs. In rapturous ecstasy, drumbeats of celebration roll – who can and will indeed dance? Then, what then? Quick, don your hat of wisdom, knowledge and understanding. Fast, seize right the opportunity to be gold and you shall become gold: VISION IS THE MASTER KEY. Are you ready? Sure pretty ready? Good, beautiful! On your mark – set, go!</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        </section>


        <!--start info service-->
        <section class="info_service">
            <div class="container">
                <div class="row sub_content">
                    <div class="col-lg-12 col-md-12 col-sm-12 wow fadeInDown">
                        <h1 class="intro text-center">Other Books by Sanmi-Ajiki</h1>
                        <p style="font-size: 20px" class="lead text-center">
                            Get Lost Into An Inspired Storyline – And You Shall Be Found!

                        </p>
                    </div>
                    <div class="rs_box  wow bounceInRight" data-wow-offset="200">
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="serviceBox_3">
                                <div class="service-icon">
                                    <i class="fa fa-book"></i>
                                </div>
                                <h3>Where Is The Gold Of Life</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad culpa dignissimos distinctio ea eius laborum nesciunt qui quis quisquam quos!</p>
                                <a class="read" href="#">Read more</a>
                            </div>
                        </div>

                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="serviceBox_3">
                                <div class="service-icon">
                                    <i class="fa fa-book"></i>
                                </div>
                                <h3>The Martyred Soldier</h3>
                                <p>This is the extraordinary story of a Man:
                                    Francis Adekunle Fajuyi – First Military Governor of Western Region of the Federal republic of Nigeria: Soldier of Soldiers, Hero Among Heroes – Saga of a Man of Peace who died by Violence…
                                </p>
                                <a class="read" href="#">Read more</a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="serviceBox_3">
                                <div class="service-icon">
                                    <i class="fa fa-book"></i>
                                </div>
                                <h3>The Adventure Of Mr Parrot</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad culpa dignissimos distinctio ea eius laborum nesciunt qui quis quisquam quos!</p>
                                <a class="read" href="#">Read more</a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="serviceBox_3">
                                <div class="service-icon">
                                    <i class="fa fa-book"></i>
                                </div>
                                <h3>Jumbo Wise Jet</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad culpa dignissimos distinctio ea eius laborum nesciunt qui quis quisquam quos!</p>
                                <a class="read" href="#">Read more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end info service-->



        <!-- Parallax & Get Quote Section -->
        <section class="parallax parallax-1">
            <div class="container">
                <!--<h2 class="center">Testimonials</h2>-->
                <div class="row">
                    <div id="parallax-testimonial-carousel" class="parallax-testimonial carousel wow fadeInUp">
                        <div class="carousel-inner">
                            <div class="active item">
                                <div class="parallax-testimonial-item">
                                    <blockquote>
                                        <p>Donec convallis, metus nec tempus aliquet, nunc metus adipiscing leo, a lobortis nisi dui ut odio. Nullam ultrices, eros accumsan vulputate faucibus, turpis tortor dictum.</p>
                                    </blockquote>
                                    <p>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </p>
                                    <div class="parallax-testimonial-review">
                                        <img src="images/testimonials/1.png" alt="testimonial">
                                        <span>Jonathan Dower</span>
                                        <small>Company Inc.</small>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="parallax-testimonial-item">
                                    <blockquote>
                                        <p>Metus aliquet tincidunt metus, sit amet mattis lectus sodales ac. Suspendisse rhoncus dictum eros, ut egestas eros luctus eget. Nam nibh sem, mattis et feugiat ut, porttitor nec risus.</p>
                                    </blockquote>
                                    <p>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </p>
                                    <div class="parallax-testimonial-review">
                                        <img src="images/testimonials/2.png" alt="testimonial">
                                        <span>Jonathan Dower</span>
                                        <small>Leopard</small>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="parallax-testimonial-item">
                                    <blockquote>
                                        <p>Nunc aliquet tincidunt metus, sit amet mattis lectus sodales ac. Suspendisse rhoncus dictum eros, ut egestas eros luctus eget. Nam nibh sem, mattis et feugiat ut, porttitor nec risus.</p>
                                    </blockquote>
                                    <p>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </p>
                                    <div class="parallax-testimonial-review">
                                        <img src="images/testimonials/3.png" alt="testimonial">
                                        <span>Jonathan Dower</span>
                                        <small>Leopard</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ol class="carousel-indicators">
                            <li data-slide-to="0" data-target="#parallax-testimonial-carousel" class=""></li>
                            <li data-slide-to="1" data-target="#parallax-testimonial-carousel" class="active"></li>
                            <li data-slide-to="2" data-target="#parallax-testimonial-carousel" class=""></li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>


        <!-- end Parallax & Get Quote Section -->

        <section id="contact" class="contact_2">
            <div class="container">
                <div class="row sub_content">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="dividerHeading">
                            <h4><span>Get in Touch</span></h4>
                        </div>
                        
                        <div class="alert alert-success hidden alert-dismissable" id="contactSuccess">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Success!</strong> Your message has been sent to us.
                        </div>

                        <div class="alert alert-error hidden" id="contactError">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Error!</strong> There was an error sending your message.
                        </div>

                        <form id="contactForm" action="" novalidate="novalidate">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-lg-6 ">
                                        <input type="text" id="name" name="name" class="form-control" maxlength="100" data-msg-required="Please enter your name." value="" placeholder="Your Name" >
                                    </div>
                                    <div class="col-lg-6 ">
                                        <input type="email" id="email" name="email" class="form-control" maxlength="100" data-msg-email="Please enter a valid email address." data-msg-required="Please enter your email address." value="" placeholder="Your E-mail" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <input type="text" id="company" name="company" class="form-control" maxlength="100" data-msg-required="Please enter the subject." value="" placeholder="Company">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="subject" name="subject" class="form-control" maxlength="100" data-msg-required="Please enter the subject." value="" placeholder="Subject">
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <textarea id="message" class="form-control" name="message" rows="10" cols="50" data-msg-required="Please enter your message." maxlength="5000" placeholder="Message"></textarea>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <input type="submit" data-loading-text="Loading..." class="btn btn-default btn-lg" value="Send Message">
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="sidebar">
                            <div class="widget_info">
                                <div class="dividerHeading">
                                    <h4><span>Contact Info</span></h4>
                                </div>
                           
                                <ul class="widget_info_contact">
                                    <li><i class="fa fa-map-marker"></i><strong>Address</strong> <p>: #2021 Lorem Ipsum<br>Delhi</p></li>
                                    <li><i class="fa fa-envelope"></i><strong>Email</strong> <p>: <a href="#">mail@example.com</a></p> <p>: <a href="#">mail@example.com</a></p></li>
                                    <li><i class="fa fa-phone"></i><strong>Our Phone</strong> <p>: (+91) 9000-12345</p><p>: (+91) 8000-54321</p></li>
                                </ul>
                            </div>

                            <div class="widget_social">
                                <ul class="widget_social">
                                    <li><a class="fb" href="#." data-placement="bottom" data-toggle="tooltip" title="Facbook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a class="twtr" href="#." data-placement="bottom" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a class="gmail" href="#." data-placement="bottom" data-toggle="tooltip" title="Google"><i class="fa fa-google-plus"></i></a></li>
                                    {{--<li><a class="dribbble" href="#." data-placement="bottom" data-toggle="tooltip" title="Dribbble"><i class="fa fa-dribbble"></i></a></li>--}}
                                    {{--<li><a class="skype" href="#." data-placement="bottom" data-toggle="tooltip" title="Skype"><i class="fa fa-skype"></i></a></li>--}}
                                    {{--<li><a class="pinterest" href="#." data-placement="bottom" data-toggle="tooltip" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>--}}
                                    {{--<li><a class="instagram" href="#." data-placement="bottom" data-toggle="tooltip" title="Instagram"><i class="fa fa-instagram"></i></a></li>--}}
                                    {{--<li><a class="youtube" href="#." data-placement="bottom" data-toggle="tooltip" title="Youtube"><i class="fa fa-youtube"></i></a></li>--}}
                                    {{--<li><a class="linkedin" href="#." data-placement="bottom" data-toggle="tooltip" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>--}}
                                    {{--<li><a class="flickrs" href="#." data-placement="bottom" data-toggle="tooltip" title="Flickr"><i class="fa fa-flickr"></i></a></li>--}}
                                    {{--<li><a class="rss" href="#." data-placement="bottom" data-toggle="tooltip" title="RSS"><i class="fa fa-rss"></i></a></li>--}}
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </section>
@endsection