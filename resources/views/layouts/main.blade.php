<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" class="no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="Sanmi-Ajiki Books, Vision is the master key, Bubbles and balloons of glory, Sanmi Ajiki, Inspirational books, Motivational books, Storyline of Gold," />
    <title>
        @yield('title', 'Sanmi-Ajiki Books')
    </title>
    <!-- CSS FILES -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/w3.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <link rel="stylesheet" href="{{asset('css/fractionslider.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/style-fraction.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/animate.css')}}"/>

    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}" media="screen" data-name="skins">
    <link rel="stylesheet" href="{{asset('css/layout/wide.css')}}" data-name="layout">
    <link rel="stylesheet" type="text/css" href="{{asset('css/switcher.css')}}" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<body>
<!--Start Header-->
@include('layouts.header')

<!--start wrapper-->
@yield('content')
<!--end wrapper-->

<!--start footer-->
@include('layouts.footer')

<script type="text/javascript" src="{{asset('js/jquery-1.10.2.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('js/retina-1.1.0.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.cookie.js')}}"></script> <!-- jQuery cookie -->
<script type="text/javascript" src="{{asset('js/styleswitch.js')}}"></script> <!-- Style Colors Switcher -->
<script src="{{asset('js/jquery.fractionslider.js')}}" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="{{asset('js/jquery.smartmenus.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.smartmenus.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.jcarousel.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jflickrfeed.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.isotope.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/swipe.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>

<!-- Start Style Switcher -->
<!-- <div class="switcher"></div> -->
<!-- End Style Switcher -->
<script>
    $(window).load(function(){
        $('.slider').fractionSlider({
            'fullWidth': 			true,
            'controls': 			true,
            'responsive': 			true,
            'dimensions': 			"1920,450",
            'timeout' :             6000,
            'increase': 			true,
            'pauseOnHover': 		true,
            'slideEndAnimation': 	false,
            'autoChange':           true
        });
    });
</script>
<!-- WARNING: Wow.js doesn't work in IE 9 or less -->
<!--[if gte IE 9 | !IE ]><!-->
<script type="text/javascript" src="{{asset('js/wow.min.js')}}"></script>
<script>
    // WOW Animation
    new WOW().init();
</script>
<![endif]-->
</body>
</html>
