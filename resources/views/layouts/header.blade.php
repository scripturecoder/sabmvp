<header id="header">
    <!-- Start info-bar -->
    <div id="info-bar">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 top-info hidden-xs">
                    <span><i class="fa fa-phone"></i>Phone: (123) 456-7890</span>
                    <span><i class="fa fa-envelope"></i>Email: wordsmithgarden@gmail.com</span>
                </div>
                <div class="col-sm-4 top-info hidden-xs">
                    <ul>
                        <li><a href="" class="my-tweet"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="" class="my-facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="" class="my-google"><i class="fa fa-google-plus"></i></a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <!--/#info-bar -->

    <div id="logo-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <div id="logo">
                        <h1 style="padding-top: 10px"><a href="{{url('/')}}"><img src="{{asset('images/sablogo.png')}}"/></a></h1>
                    </div>
                </div>
                <div class="col-md-9 col-sm-9">
                    <!-- Navigation
                    ================================================== -->
                    <div class="navbar navbar-default navbar-static-top" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="{{url('/')}}">Home</a></li>
                                <li><a href="#about">About</a></li>
                                <li><a href="#blob">Blob</a></li>
                                <li><a href="#contact">Contact</a></li>
                                <li><a href="#quote">Quotes</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/.container -->
    </div>
    <!--/#logo-bar -->
</header>
<!--End Header-->