@extends("book.layouts.main")
@section('title','')
@section('content')

    <div class="w3-container w3-center">
        <img src="{{asset('images/book.png')}}" alt="">
    </div>
    <div class="w3-container">
    	<div><p class="MsoNormalCxSpFirst" align="center" style="margin-top:0in;margin-right:
-31.5pt;margin-bottom:10.0pt;margin-left:-22.5pt;mso-add-space:auto;text-align:
center;line-height:normal"><b><span lang="EN-GB" style="font-size:48.0pt;font-family:&quot;Bernard MT Condensed&quot;,&quot;serif&quot;">Bubbles<o:p></o:p></span></b></p>

<p class="MsoNormalCxSpMiddle" align="center" style="margin-top:0in;margin-right:
-31.5pt;margin-bottom:10.0pt;margin-left:-22.5pt;mso-add-space:auto;text-align:
center;line-height:normal"><b><i><span lang="EN-GB" style="font-size:48.0pt;
font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">and<o:p></o:p></span></i></b></p>

<p class="MsoNormalCxSpMiddle" align="center" style="margin-top:0in;margin-right:
-31.5pt;margin-bottom:10.0pt;margin-left:-22.5pt;mso-add-space:auto;text-align:
center;line-height:normal"><b><span lang="EN-GB" style="font-size:56.5pt;font-family:&quot;Bernard MT Condensed&quot;,&quot;serif&quot;">Balloons<o:p></o:p></span></b></p>

<p class="MsoNormalCxSpMiddle" align="center" style="margin-top:0in;margin-right:
-31.5pt;margin-bottom:10.0pt;margin-left:-22.5pt;mso-add-space:auto;text-align:
center;line-height:normal"><b><i><span lang="EN-GB" style="font-size:48.0pt;
font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">of</span></i></b><b><span lang="EN-GB" style="font-size:48.0pt;font-family:&quot;Bernard MT Condensed&quot;,&quot;serif&quot;"><o:p></o:p></span></b></p>

<p class="MsoNormalCxSpMiddle" align="center" style="margin-top:0in;margin-right:
-31.5pt;margin-bottom:10.0pt;margin-left:-22.5pt;mso-add-space:auto;text-align:
center;line-height:normal"><span lang="EN-GB" style="font-size:65.0pt;font-family:
Algerian">GLoRY...<o:p></o:p></span></p>

<p class="MsoNormalCxSpMiddle" align="center" style="margin-top:0in;margin-right:
-31.7pt;margin-bottom:10.0pt;margin-left:-22.5pt;mso-add-space:auto;text-align:
center;line-height:normal"><span lang="EN-GB" style="font-size:24.0pt;font-family:
&quot;Bernard MT Condensed&quot;,&quot;serif&quot;">****************************************</span><span lang="EN-GB" style="font-size:24.0pt;font-family:Algerian"><o:p></o:p></span></p>

<p class="MsoNormalCxSpMiddle" align="center" style="margin-top:0in;margin-right:
-31.7pt;margin-bottom:10.0pt;margin-left:-22.5pt;mso-add-space:auto;text-align:
center;line-height:normal"><b><span lang="EN-GB" style="font-size:28.0pt;font-family:&quot;Bernard MT Condensed&quot;,&quot;serif&quot;">(</span></b><b><span lang="EN-GB" style="font-size:28.0pt;
font-family:Jokerman">Sparkles</span></b><b><span lang="EN-GB" style="font-size:28.0pt;font-family:&quot;Bernard MT Condensed&quot;,&quot;serif&quot;">
</span></b><b><i><span lang="EN-GB" style="font-size:28.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">of
</span></i></b><span lang="EN-GB" style="font-size:28.0pt;font-family:Magneto">Paradise</span><b><span lang="EN-GB" style="font-size:28.0pt;
font-family:&quot;Bernard MT Condensed&quot;,&quot;serif&quot;"> </span></b><b><i><span lang="EN-GB" style="font-size:28.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">in the</span></i></b><b><i><span lang="EN-GB" style="font-size:28.0pt;font-family:&quot;Bernard MT Condensed&quot;,&quot;serif&quot;">
</span></i></b><b><span lang="EN-GB" style="font-size:28.0pt;font-family:&quot;Matura MT Script Capitals&quot;">Wind!</span></b><b><span lang="EN-GB" style="font-size:28.0pt;
font-family:&quot;Bernard MT Condensed&quot;,&quot;serif&quot;">)</span></b><b><span lang="EN-GB" style="font-size:24.0pt;font-family:&quot;Bernard MT Condensed&quot;,&quot;serif&quot;"><o:p></o:p></span></b></p>

<p class="MsoNormalCxSpMiddle" align="center" style="margin-top:0in;margin-right:
-31.7pt;margin-bottom:10.0pt;margin-left:-22.5pt;mso-add-space:auto;text-align:
center;line-height:normal"><span lang="EN-GB" style="font-size:24.0pt;font-family:
&quot;Bernard MT Condensed&quot;,&quot;serif&quot;">****************************************<o:p></o:p></span></p>

<p class="MsoNormalCxSpMiddle" align="center" style="margin-top:0in;margin-right:
-31.7pt;margin-bottom:10.0pt;margin-left:-22.5pt;mso-add-space:auto;text-align:
center;line-height:normal"><span lang="EN-GB" style="font-size:9.0pt;font-family:
&quot;Bernard MT Condensed&quot;,&quot;serif&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormalCxSpMiddle" align="center" style="margin-top:0in;margin-right:
-31.7pt;margin-bottom:10.0pt;margin-left:-22.5pt;mso-add-space:auto;text-align:
center;line-height:normal"><span lang="EN-GB" style="font-size:9.0pt;font-family:
&quot;Bernard MT Condensed&quot;,&quot;serif&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormalCxSpMiddle" align="center" style="margin-top:0in;margin-right:
-31.7pt;margin-bottom:10.0pt;margin-left:-22.5pt;mso-add-space:auto;text-align:
center;line-height:normal"><span lang="EN-GB" style="font-size:9.0pt;font-family:
&quot;Bernard MT Condensed&quot;,&quot;serif&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormalCxSpMiddle" align="center" style="margin-top:0in;margin-right:
-31.7pt;margin-bottom:10.0pt;margin-left:-22.5pt;mso-add-space:auto;text-align:
center;line-height:normal"><span lang="EN-GB" style="font-size:9.0pt;font-family:
&quot;Bernard MT Condensed&quot;,&quot;serif&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormalCxSpMiddle" align="center" style="margin-top:0in;margin-right:
-31.7pt;margin-bottom:10.0pt;margin-left:-22.5pt;mso-add-space:auto;text-align:
center;line-height:normal"><span lang="EN-GB" style="font-size:9.0pt;font-family:
&quot;Bernard MT Condensed&quot;,&quot;serif&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormalCxSpMiddle" align="center" style="margin-top:0in;margin-right:
-31.7pt;margin-bottom:10.0pt;margin-left:-22.5pt;mso-add-space:auto;text-align:
center;line-height:normal"><b><span lang="EN-GB" style="font-size:38.0pt;font-family:&quot;Bernard MT Condensed&quot;,&quot;serif&quot;">Sanmi-Ajiki<o:p></o:p></span></b></p>

<p class="MsoNormalCxSpMiddle" align="center" style="margin-top:0in;margin-right:
-31.5pt;margin-bottom:10.0pt;margin-left:-22.5pt;mso-add-space:auto;text-align:
center;line-height:normal"><b><span lang="EN-GB" style="font-size:3.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________<o:p></o:p></span></b></p>

<p class="MsoNormalCxSpMiddle" align="center" style="margin-top:0in;margin-right:
-13.5pt;margin-bottom:10.0pt;margin-left:-22.5pt;mso-add-space:auto;text-align:
center;line-height:normal"><b><span lang="EN-GB" style="font-size:24.0pt;
font-family:&quot;Bernard MT Condensed&quot;,&quot;serif&quot;">Deep Stories Of Life By Time Series<span style="background:yellow;mso-highlight:yellow"> <o:p></o:p></span></span></b></p></div>
        <button class="w3-btn w3-green">Start Reading</button>
    </div>

@endsection