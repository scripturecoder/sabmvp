<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Book
 *
 * @package App
 * @property string $name
 * @property text $description
 * @property decimal $price
*/
class Book extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'description', 'price'];
    protected $hidden = [];
    
    
    public static function boot()
    {
        parent::boot();

        Book::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setPriceAttribute($input)
    {
        $this->attributes['price'] = $input ? $input : null;
    }
    
}
