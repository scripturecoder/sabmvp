<?php

$factory->define(App\Book::class, function (Faker\Generator $faker) {
    return [
        "name" => $faker->name,
        "description" => $faker->name,
        "price" => $faker->randomNumber(2),
    ];
});
