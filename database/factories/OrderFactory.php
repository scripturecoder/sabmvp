<?php

$factory->define(App\Order::class, function (Faker\Generator $faker) {
    return [
        "user_id" => $faker->randomNumber(2),
        "book_id" => $faker->randomNumber(2),
        "quantity" => $faker->randomNumber(2),
    ];
});
